#!/usr/bin/env python

import sys, argparse, configparser

from datetime import datetime, date
from zoneinfo import ZoneInfo
from astropy import units as u
from astropy.time import Time, TimeDelta
from astropy.coordinates import EarthLocation
from astroplan import Observer
from astropy.utils.masked import Masked
import numpy as np
from astropy.io import ascii

from astropy.table import QTable

import warnings

## TO DO: how ignore ONLY "TargetNeverUpWarning"???
warnings.filterwarnings("ignore")

def time_floor_minutes(time, step):
    minutes=int(time.strftime('%M'))
    new_minutes=minutes-minutes%step
    return Time.strptime(time.strftime('%Y-%m-%d-%H-')+str(new_minutes), '%Y-%m-%d-%H-%M')

def time_ceil_minutes(time, step):
    return time_floor_minutes(time, step)+step*u.minute

def find_time_limits(observer, time, tstep, altmin, altmax):

    while True:
        risetime1=observer.moon_rise_time(time, which='next', horizon=altmin*u.degree)
        if risetime1.masked:
            time=time+24*u.hour
        else:
            break
    risetime2=observer.moon_rise_time(risetime1, which='next', horizon=altmax*u.degree)
    if risetime2.masked:
        time=risetime1
    else:
        settime1=observer.moon_set_time(risetime2, which='next', horizon=altmax*u.degree)
        time=settime1
    settime2=observer.moon_set_time(time, which='next', horizon=altmin*u.degree)
    if risetime2.masked or settime1.masked:
        time_limits=[time_ceil_minutes(risetime1, tstep), time_floor_minutes(settime2, tstep)]
    else:
        time_limits=[time_ceil_minutes(risetime1, tstep), time_floor_minutes(risetime2, tstep), time_ceil_minutes(settime1, tstep), time_floor_minutes(settime2, tstep)]

    return time_limits

def compute_times(observer, ini_time, end_time, tstep):

    # Thanks to Jorge Perez Prieto for point to use
    # a TimeDelta object here
    stop = (end_time.jd-ini_time.jd)*24*60/tstep
    step = TimeDelta(tstep*60, format='sec')
    times = ini_time + (step*np.arange(0,stop))
    return times

def compute_mask(altazs, azmin, azmax):
    # return 3 elements tuple
    # 1- index first valid time
    # 2- index max alt in valid time
    # 3- index last valid time
    if (altazs.az[0]>azmax): return (None, None, None)
    for idx_ini, val in enumerate(altazs.az):
        if val>=azmin:
            break
    for idx_end, val in enumerate(altazs.az[idx_ini:]):
        if val>=azmax:
            break
    idx_end+=idx_ini-1
    if(idx_ini == idx_end):
         idx_max=idx_ini
    else:
        idx_max=np.argmax(altazs.alt[idx_ini:idx_end+1])
    idx_max+=idx_ini

    return (idx_ini, idx_max, idx_end)

def config2file(ini_date, c):
    f = open("config.tex", "w")

    f.write('Sitio: {}\n\n'.format(c['name']))
    f.write('Latitud: {} \\qquad Longitud: {} \\qquad Altura: {}\n\n'.format(c['latitude'],c['longitude'],c['altitude']))
    f.write('Timezone: {}\n\n'.format(c['timezone']))
    f.write('Altura min.: {} \\qquad Altura max.: {} \\qquad Azimut min.: {} \\qquad Azimut max.: {}\n\n'.format(c['altmin'],c['altmax'],c['azmin'],c['azmax']))
    f.write('Fecha inicio: {} \\qquad Durante: {} días \\qquad Resolución de tiempo: {} minutos\n\n'.format(ini_date, c['days'],c['every_minutes']))

    f.close()

###############################################################################
if __name__=='__main__':

    def get_conf(ini_file, section):

        mandatory_keys={'name':str, 'timezone':str, 'days':int, \
            'every_minutes':int, 'latitude':float, 'longitude':float, \
            'altitude':float, 'altmin':float, 'altmax':float, 'azmin':float, \
            'azmax':float}
        optional_keys={'description':str}

        config = configparser.ConfigParser()
        try:
            f = open(ini_file, "r")
            config.read_file(f)
            f.close()
        except:
            print('ERROR: procesing file \"{}\". It is exists?'.format(ini_file))
            return None

        if section not in config.sections():
            print('ERROR: Site \"{}\" does not exists'.format(section))
            print('       Available sites: {}'.format(config.sections()))
            return None

        found_keys=[]
        unknown_keys=[]
        missing_keys=[]

        for key in config[section]:
            if (key not in mandatory_keys.keys()) and (key not in optional_keys.keys()):
                unknown_keys.append(key)
            else:
                found_keys.append(key)

        if not all([key in found_keys for key in mandatory_keys.keys()]):
            for val in mandatory_keys.keys():
                if val not in found_keys: missing_keys.append(val)

        if (0 != len(unknown_keys)) or (0 != len(missing_keys)):
            if 0 != len(unknown_keys):
                print('ERROR: Unknown keys in section {}: {}'.format(section, unknown_keys))
            if 0 != len(missing_keys):
                print('ERROR: Missing keys in section {}: {}'.format(section, missing_keys))
            return None
        else:
                    config_dict=dict(config[section])
        result={}
        for key, value in mandatory_keys.items():
            try:
                result[key]=value(config_dict[key])
            except:
                print('ERROR: Type of key {} must be '.format(key), value)
        for key, value in optional_keys.items():
            try:
                result[key]=value(config_dict[key])
            except:
                print('ERROR: Type of key {} must be '.format(key), value)

        if len(result)==len(config_dict): return result
        else: return None

############## MAIN ###################

    DEFAULT_CONF_FILE='moon_window.ini'
    DEBUG=False

    parser = argparse.ArgumentParser()
    parser.add_argument("site", help="Site (as section from configuration file)", type=str)
    parser.add_argument("init_date", help="Initial date for predictions", type=str)
    parser.add_argument("-f", "--conf_file", help="Configuration file", type=str)
    args = parser.parse_args()
    if args.conf_file!=None: conf_file=args.conf_file
    else: conf_file=DEFAULT_CONF_FILE


    config = get_conf(conf_file, args.site)
    if config==None:
        print('ERROR: Unable to continue')
        sys.exit(1)
    if(DEBUG):
        print('CONFIG: ')
        print(config)
        print('\n')
    else:
        config2file(args.init_date, config)

    #TIME:
    try:
        localtzinfo=ZoneInfo(config['timezone'])
    except:
        print('ERROR: No valid timezone: \"{}\"'.format(config['timezone']))
        sys.exit(1)
    try:
        firstlocaldate_tmp=date.fromisoformat(args.init_date)
    except:
        print('ERROR: No valid initial date in ISO format (yyy-mm-dd): \"{}\"'.format(args.init_date))
        sys.exit(1)
    firstlocaldate=datetime.strptime(firstlocaldate_tmp.strftime('%Y%m%d'), '%Y%m%d').astimezone(localtzinfo)
    firstdate=Time(firstlocaldate)

    #Observer:
    location=EarthLocation(lat=config['latitude']*u.deg, lon=config['longitude']*u.deg, height=config['altitude']*u.m)
    observer=Observer(name=config['name'], location=location, timezone=config['timezone'])

    time_limits_for_periods=[]
    time=firstdate
    for _ in range(config['days']):
        time_limits_for_periods=time_limits_for_periods+find_time_limits(observer, time, config['every_minutes'], config['altmin'], config['altmax'])
        time=time_limits_for_periods[-1]

    times=[compute_times(observer, val1, val2, config['every_minutes']) for val1, val2 in zip(time_limits_for_periods[0::2],time_limits_for_periods[1::2])]


    altazs=[observer.moon_altaz(time) for time in times]

    masks=[compute_mask(altaz, config['azmin']*u.deg, config['azmax']*u.deg) for altaz in altazs]


###############################################################################
    if(DEBUG):
        for i, time, altaz in zip (masks, times, altazs):
            print('\nTupla de índices: {}'.format(i))
            tmp=0
            for t, a in zip(time, altaz):
                print('========== IDX: {} ========='.format(tmp))
                print('Time: ', t)
                print('Coor: ', a.alt, a.az)
                tmp+=1

#######################################################3

    print_format=' %Y-%m-%d  %H:%M '
    table=QTable(names=['Start', 'Max Alt', 'End'], dtype=[str,str,str])

    for mask, altaz in zip (masks, altazs):
        if mask[0]==None: continue

        start_time=altaz.obstime[mask[0]].to_datetime(observer.timezone).strftime(print_format)
        best_time=altaz.obstime[mask[1]].to_datetime(observer.timezone).strftime(print_format)
        end_time=altaz.obstime[mask[2]].to_datetime(observer.timezone).strftime(print_format)
        table.add_row([start_time, best_time, end_time])

#    htmldict={'table_class':'w3-responsive'}
#    my_latexdict={}
    if(DEBUG):
        table.show_in_browser()
        ascii.write(table, Writer=ascii.Latex, latexdict=ascii.latex.latexdicts['template'])

    else: ascii.write(table, output='tabla.tex', overwrite=True, Writer=ascii.Latex, latexdict=ascii.latex.latexdicts['AA'])
     
##https://docs.python.org/3/library/zoneinfo.html
##https://docs.python.org/3/library/argparse.html
##https://docs.python.org/3/library/getopt.html
